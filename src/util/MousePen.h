#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "Global.h"
#include "GLM/glm.hpp"
#include <vector>
#include "VertexBuffer.h"
#include "VertexArray.h"
#include "Shader.h"

class MousePen
{
	glm::mat4 model;
public:
	MousePen();
	static std::vector<glm::vec2> pos;
	static VertexArray* va;
	/*static VertexBuffer* vb;

	static Shader shader;

	static double mouseX;
	static double mouseY;
	static bool isClicked;

	static void mouseMoveEvent(GLFWwindow* window, double x, double y);
	static void mouseClickEvent(GLFWwindow* window, int button, int action, int mods);*/

	void setMouseMoveEvent(GLFWwindow* window);
	void setMouseClickEvent(GLFWwindow* window);

	void draw();

	void Bind();

};