#include "VertexArray.h"
#include <GL/glew.h>
#include "Debug.h"

VertexArray::VertexArray() : vb(NULL), ib(NULL) {
	glGenVertexArrays(1, &m_id);
	glBindVertexArray(m_id);
}

VertexArray::~VertexArray() {
	glDeleteVertexArrays(1, &m_id);
}

void VertexArray::Bind() {
	glBindVertexArray(m_id);
}

void VertexArray::Unbind() {
	glBindVertexArray(0);
}

void VertexArray::AddBuffer(VertexBuffer& vb, VertexBufferLayout& layout) {
	this->vb = &vb;
	Bind();
	vb.Bind();
	const auto& elements = layout.GetElements();
	unsigned int offset = 0;
	for (int i = 0; i < elements.size(); i++) {
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, elements[i].count, elements[i].type, elements[i].normalized, layout.GetStride(), (const void*)offset);
		offset += elements[i].count * VertexBufferElement::GetSizeOfType(elements[i].type);


	}

}

void VertexArray::AddIndex(IndexBuffer& ib) {
	this->ib = &ib;
	Bind();
	ib.Bind();
}
