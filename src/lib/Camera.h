#pragma once
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>

class Camera
{
public:
	glm::mat4 proj;
	glm::mat4 view;
	Camera(float width, float height);
	Camera();
	~Camera();

	void SetProj(float width, float height);
	void SetView(glm::mat4 viu);
};

