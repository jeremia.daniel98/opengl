#pragma once

#include "Camera.h"



static Camera mainCamera;

struct vector2f {
	float x, y;
};

struct vector3f {
	float x, y, z;
};

struct vector2i {
	int x, y;
};

struct vector3i {
	int x, y, z;
};