#include "Camera.h"

Camera::Camera(float width, float height)
	: proj(glm::ortho(0.0f, width, 0.0f, height, -1.0f, 1.0f)), view(glm::mat4(1.0f)) {

}

Camera::Camera()
	: proj(glm::ortho(0.0f, 800.0f, 0.0f, 600.0f, -1.0f, 1.0f)), view(glm::mat4(1.0f))
{

}

Camera::~Camera() {

}

void Camera::SetProj(float width, float height) {
	proj = glm::ortho(0.0f, width, 0.0f, height, -1.0f, 1.0f);
}

void Camera::SetView(glm::mat4 viu)
{
	view = viu;
}
