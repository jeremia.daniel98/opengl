#pragma once
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "VertexBufferLayout.h"

class VertexArray {
	unsigned int m_id;
	VertexBuffer* vb;
	IndexBuffer* ib;
public:
	VertexArray();
	~VertexArray();

	void Bind();
	void Unbind();
	void AddBuffer(VertexBuffer& vb, VertexBufferLayout& layout);
	void AddIndex(IndexBuffer& ib);
};

