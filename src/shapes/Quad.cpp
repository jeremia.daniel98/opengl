#include "Quad.h"

Quad::Quad(float posx, float posy, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
{
	vertex_count = 4;
	setPos(posx, posy);
	va.Bind();

	float vertices[8];
	vertices[0] = origin.x + x1;
	vertices[1] = origin.y + y1;

	vertices[2] = origin.x + x2;
	vertices[3] = origin.y + y2;

	vertices[4] = origin.x + x3;
	vertices[5] = origin.y + y3;

	vertices[6] = origin.x + x4;
	vertices[7] = origin.y + y4;

	unsigned int indices[4] = {
		0, 1, 2, 3
	};

	vb = new VertexBuffer(vertices, 4 * 2 * sizeof(float));
	VertexBufferLayout layout;

	ib = new IndexBuffer(indices, 4);
	layout.Push<float>(2);
	va.AddBuffer(*vb, layout);
	va.AddIndex(*ib);
}

Quad::~Quad()
{
	
}

void Quad::update()
{
	Shape::update();
}

void Quad::draw()
{
	update();
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, nullptr);
}
