#pragma once
#include "Shape.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "IndexBuffer.h"
class Rect : public Shape
{

public:
	Rect(float posx, float posy, float width, float height);
	~Rect();

	void update() override;
	void draw() override;

};

