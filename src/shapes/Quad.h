#pragma once
#include "Shape.h"
class Quad :
    public Shape
{
public:
	Quad(float posx, float posy, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);
	~Quad();

	void update() override;
	void draw() override;
};

