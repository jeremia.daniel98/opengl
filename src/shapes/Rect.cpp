#include "Rect.h"
#include <iostream>
Rect::Rect(float posx, float posy, float width, float height) {
	vertex_count = 4;
	setPos(posx, posy);
	setSize(width, height);
	va.Bind();

	float vertices[8];
	vertices[0] = origin.x;
	vertices[1] = origin.y;

	vertices[2] = origin.x + width;
	vertices[3] = origin.y;

	vertices[4] = origin.x + width;
	vertices[5] = origin.y + height;

	vertices[6] = origin.x;
	vertices[7] = origin.y + height;

	unsigned int indices[6] = {
		0, 1, 2,
		0, 3, 2
	};

	vb = new VertexBuffer(vertices, 4 * 2 * sizeof(float));
	VertexBufferLayout layout;

	ib = new IndexBuffer(indices, 6);
	layout.Push<float>(2);
	va.AddBuffer(*vb, layout);
	va.AddIndex(*ib);
}

Rect::~Rect(){

}

void Rect::update() {
	Shape::update();
}

void Rect::draw() {
	update();
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
}
