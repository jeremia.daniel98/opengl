#pragma once
#include "Shape.h"
#include <math.h>
class Circle :
    public Shape
{
	float radius;
public:
	Circle(float posx, float posy, float radius);
	~Circle();

	void update() override;
	void draw() override;
};

