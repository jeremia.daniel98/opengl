#include "Circle.h"

Circle::Circle(float posx, float posy, float radius)
{
	vertex_count = 1100;
	setPos(posx, posy);
	this->radius = radius;
	va.Bind();

	float vertices[1086];
	int x = 0, y = 1, z = 2;
	int sudut = 360;
	for (int i = 0; i <= sudut; i++) {
		float s = i * (2 * 3.14 / 360);
		vertices[x] = (radius * cos(s));
		vertices[y] = (radius * sin(s));
		vertices[z] = 0.0;
		x = x + 3; y = y + 3; z = z + 3;
	}

	vb = new VertexBuffer(vertices, 1086 * 3 * sizeof(float));
	VertexBufferLayout layout;
	layout.Push<float>(3);
	va.AddBuffer(*vb, layout);
}

Circle::~Circle()
{
	
}

void Circle::update()
{
	Shape::update();
}

void Circle::draw()
{
	update();
	glDrawArrays(GL_POLYGON, 0, 360);
}
