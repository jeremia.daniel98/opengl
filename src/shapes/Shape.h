#pragma once
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Shader.h"
#include "Global.h"

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <string>

class Shape
{
protected:
	VertexArray va;
	VertexBuffer* vb;
	IndexBuffer* ib;
	Shader shader;

	int vertex_count;

	glm::vec2 origin;
	float width;
	float height;

	glm::vec2 position;
	float scale;
	float rotation;

	glm::vec4 color;

	glm::mat4 model_matrix;


	virtual void resetModel() {
		model_matrix = glm::mat4(1.f);
	}
public:
	Shape() : shader("src/shaders/Basic.shader"), origin(glm::vec2(0.f)), 
		model_matrix(glm::mat4(1.f)), color(glm::vec4(1.f)), scale(1.0f), rotation(0.0f){}
	~Shape() {}

	virtual void update() {
		Bind();
		calc_model();
		uniforms();
	}
	virtual void draw() = 0;

	virtual void setPos(float x, float y) {
		position.x = x;
		position.y = y;
	}

	virtual void setSize(float width, float height) {
		this->width = width;
		this->height = height;
	}

	virtual void setScale(float x) {
		scale = x;
	}

	virtual void setOrigin(float x, float y) {
		origin.x = x;
		origin.y = y;
	}

	virtual void setColor(float r, float g, float b, float a) {
		color.r = r;
		color.g = g;
		color.b = b;
		color.a = a;
	}

	virtual void setColor(std::string hex, float alpha) {
		float r = 1.f, g = 1.f, b = 1.f;
		if (hex.length() == 7) {
			std::string red = "";
			red += hex[1];
			red += hex[2];
			r = std::stoi(red, nullptr, 16) / 255.f;

			std::string green = "";
			green += hex[3];
			green += hex[4];
			g = std::stoi(green, nullptr, 16) / 255.f;

			std::string blue = "";
			blue += hex[5];
			blue += hex[6];
			b = std::stoi(blue, nullptr, 16) / 255.f;
		}
		color.r = r;
		color.g = g;
		color.b = b;
		color.a = alpha;
	}

	virtual void setRotation(float angleInDegrees) {
		rotation = angleInDegrees;
	}
	
	virtual void Bind() {
		va.Bind();
		shader.Bind();
	}

	virtual void calc_model() {
		model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(position.x, position.y, 0.0f))
		*glm::rotate(glm::mat4(1.f), rotation, glm::vec3(0.0f, 0.0f, 1.0f))
			* glm::scale(glm::mat4(1.f), glm::vec3(scale, scale, 0.0f));
		glm::mat4 proj = mainCamera.proj;
		glm::mat4 view = mainCamera.view;
		glm::vec4 test = mainCamera.proj * mainCamera.view * model_matrix * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	virtual void uniforms() {
		shader.Bind();
		shader.SetUniform4f("u_Color", color.r, color.g, color.b, color.a);
		shader.SetUniformMat4f("u_cam", mainCamera.proj * mainCamera.view);
		shader.SetUniformMat4f("u_model", model_matrix);
	}

	virtual void move(float x, float y) {
		position.x += x;
		position.y += y;
	}
};

