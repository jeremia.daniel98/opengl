#pragma once
#include "Shape.h"
class Triangle :
    public Shape
{
public:
	enum {
		TRI_RIGHTANGLE_LEFT, TRI_RIGHTANGLE_RIGHT, TRI_STRAIGHT
	};

	Triangle(float posx, float posy, float x1, float y1, float x2, float y2, float x3, float y3);
	Triangle(float posx, float posy, float base, float height, int mode);
	~Triangle();

	void update() override;
	void draw() override;
};

