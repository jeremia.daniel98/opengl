#include "Triangle.h"

Triangle::Triangle(float posx, float posy, float x1, float y1, float x2, float y2, float x3, float y3)
{
	vertex_count = 3;
	setPos(posx, posy);
	va.Bind();

	float vertices[6];
	vertices[0] = origin.x + x1;
	vertices[1] = origin.y + y1;

	vertices[2] = origin.x + x2;
	vertices[3] = origin.y + y2;

	vertices[4] = origin.x + x3;
	vertices[5] = origin.y + y3;

	unsigned int indices[3] = {
		0, 1, 2
	};

	vb = new VertexBuffer(vertices, 3 * 2 * sizeof(float));
	VertexBufferLayout layout;

	ib = new IndexBuffer(indices, 3);
	layout.Push<float>(2);
	va.AddBuffer(*vb, layout);
	va.AddIndex(*ib);
}

Triangle::Triangle(float posx, float posy, float base, float height, int mode)
{
	vertex_count = 3;
	setPos(posx, posy);
	va.Bind();

	float vertices[6];

	switch (mode)
	{
	case TRI_RIGHTANGLE_LEFT:
		vertices[0] = origin.x;
		vertices[1] = origin.y;

		vertices[2] = origin.x;
		vertices[3] = origin.y + height;

		vertices[4] = origin.x + base;
		vertices[5] = origin.y;
		break;
	case TRI_RIGHTANGLE_RIGHT:
		vertices[0] = origin.x;
		vertices[1] = origin.y;

		vertices[2] = origin.x + base;
		vertices[3] = origin.y;

		vertices[4] = origin.x + base;
		vertices[5] = origin.y + height;
		break;
	case TRI_STRAIGHT:
		vertices[0] = origin.x;
		vertices[1] = origin.y;

		vertices[2] = origin.x + base;
		vertices[3] = origin.y;

		vertices[4] = origin.x + base/2;
		vertices[5] = origin.y + height;
		break;
	}
	

	unsigned int indices[3] = {
		0, 1, 2
	};

	vb = new VertexBuffer(vertices, 3 * 2 * sizeof(float));
	VertexBufferLayout layout;

	ib = new IndexBuffer(indices, 3);
	layout.Push<float>(2);
	va.AddBuffer(*vb, layout);
	va.AddIndex(*ib);
}

Triangle::~Triangle()
{

}

void Triangle::update()
{
	Shape::update();
}

void Triangle::draw()
{
	update();
	glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, nullptr);
}
