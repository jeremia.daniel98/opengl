#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Global.h"

#include <iostream>
#include <string>
#include <vector>

#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "VertexArray.h"
#include "IndexBuffer.h"
#include "Shader.h"
#include "Rect.h"
#include "Triangle.h"
#include "Circle.h"
#include "Quad.h"
#include "MousePen.h"


class Application {
    GLFWwindow* window;
	unsigned int WINDOW_HEIGHT;
	unsigned int WINDOW_WIDTH;
	std::string TITLE;

	Camera camera;
	MousePen* mousePen;

	
public:
	
    Application(unsigned int width = 800, unsigned int height = 600, std::string title = "My Application") :
		window(NULL), WINDOW_WIDTH(width), WINDOW_HEIGHT(height), TITLE(title){
		/* Initialize the library */
        if (!glfwInit())
            exit(-1);

		/* Create a windowed mode window and its OpenGL context */
		window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, TITLE.c_str(), NULL, NULL);
		if (!window) {
			glfwTerminate();
            exit(-1);
		}

		/* Make the window's context current */
		glfwMakeContextCurrent(window);

		if (glewInit() != GLEW_OK) {
			std::cout << "Glew Init Failed" << std::endl;
            exit(-1);
		}
		
		glfwSwapInterval(1);
		camera.SetProj(WINDOW_WIDTH, WINDOW_HEIGHT);
		camera.SetView(glm::mat4(1.f));
		mainCamera = camera;
		mousePen = new MousePen();
		mousePen->setMouseClickEvent(window);
		mousePen->setMouseMoveEvent(window);
		glEnable(0x8642);
    }

    ~Application() {
        glfwTerminate();
    }

	


    void run() {
		/*float positions[8] = {
			0.0f, 0.0f,
			100.0f, 0.0f,
			100.0f, 100.0f,
			0.0f, 100.0f
		};

		unsigned int indices[] = {
			0, 1, 2,
			0, 3, 2
		};

		VertexArray va;
		VertexBuffer vb(positions, 8 * sizeof(float));
		VertexBufferLayout layout;
		layout.Push<float>(2);
		va.AddBuffer(vb, layout);
		IndexBuffer ib(indices, 6);
		va.AddIndex(ib);

		Shader shader("src/shaders/Basic.shader");
		shader.SetUniform4f("u_Color", 1.f, 1.f, 1.f, 1.f);
		shader.SetUniformMat4f("u_cam", mainCamera.proj * mainCamera.view);

		glm::mat4 model = glm::translate(glm::mat4(1.f), glm::vec3(400, 300, 0));
		shader.SetUniformMat4f("u_model", model);*/

		Rect grass(0.0f, 0.0f, WINDOW_WIDTH, WINDOW_HEIGHT / 3);
		grass.setColor("#659e57", 1.0f);

		Triangle left_mountain(175.f, WINDOW_HEIGHT / 3, 300.f, 250.f, Triangle::TRI_STRAIGHT);
		left_mountain.setColor("#8795b0", 1.0f);

		Triangle right_mountain(325.f, WINDOW_HEIGHT / 3, 300.f, 250.f, Triangle::TRI_STRAIGHT);
		right_mountain.setColor("#7b899e", 1.0f);

		Quad street(150.f, 0.f, 0.0f, 0.0f, 300.f, 0.0f, 200.f, WINDOW_HEIGHT / 3.f, 175.f, WINDOW_HEIGHT / 3.f);
		street.setColor("#ead0a8", 1.f);

		Rect wall(500.f, 50.f, 200.f, 200.f);
		wall.setColor("#dddddd", 1.0f);

		Triangle roof(475.f, 250.f, 250.f, 100.f, Triangle::TRI_STRAIGHT);
		roof.setColor("#ffc0cb", 1.0f);

		Rect door(530.f, 50.f, 60.f, 130.f);
		door.setColor("#51443c", 1.0f);

		Rect jendela(620.f, 110.f, 50.f, 50.f);
		jendela.setColor("#51443c", 1.0f);

		Rect bl(622.5f, 112.5f, 22.0f, 22.0f);
		Rect tr(646.f, 136.f, 22.0f, 22.0f);
		Rect tl(622.5f, 136.f, 22.0f, 22.0f);
		Rect br(646.f, 112.5f, 22.0f, 22.0f);

		Rect chimney(630.f, 225.f, 50.f, 125.f);
		chimney.setColor("#ffd264", 1.f);

		Circle moon(620.f, 500.f, 75.f);
		moon.setColor("#ffff00", 1.f);

		Circle cut(600.f, 525.f, 90.f);
		cut.setColor(8.f / 255.f, 43.0f / 255.f, 94.0f / 255.f, 1.0f);

		Rect wood(75.f, 50.f, 50.f, 200.f);
		wood.setColor("#51443c", 1.f);

		Triangle leaf1(25.f, 120.f, 150.f, 100.f, Triangle::TRI_STRAIGHT);
		Triangle leaf2(25.f, 150.f, 150.f, 100.f, Triangle::TRI_STRAIGHT);
		Triangle leaf3(25.f, 180.f, 150.f, 100.f, Triangle::TRI_STRAIGHT);
		Triangle leaf4(25.f, 210.f, 150.f, 100.f, Triangle::TRI_STRAIGHT);
		leaf1.setColor("#087830", 1.0f);
		leaf2.setColor("#087830", 1.0f);
		leaf3.setColor("#087830", 1.0f);
		leaf4.setColor("#087830", 1.0f);

		float r = 0.1f, g = 0.3, b = 0.2f;
		float dr = 0.05f, dg = 0.05f, db = 0.05f;

		/* Loop until the user closes the window */
		while (!glfwWindowShouldClose(window)) {

			/* Render here */
			glClearColor(8.f/255.f, 43.0f/255.f, 94.0f/255.f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			/*va.Bind();
			shader.Bind();
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);*/
			
			moon.draw();
			cut.draw();
			grass.draw();
			right_mountain.draw();
			left_mountain.draw();

			street.draw();

			wood.draw();
			leaf1.draw();
			leaf2.draw();
			leaf3.draw();
			leaf4.draw();

			chimney.draw();
			wall.draw();
			roof.draw();
			door.draw();
			jendela.draw();

			tl.setColor(r, g, b, 1.0f);
			tr.setColor(r, g, b, 1.0f);
			bl.setColor(r, g, b, 1.0f);
			br.setColor(r, g, b, 1.0f);

			tl.draw();
			tr.draw();
			bl.draw();
			br.draw();

			if (r <= 0.0f)
				dr = 0.05f;
			else if (r >= 1.f)
				dr = -0.05f;

			if (g <= 0.0f)
				dg = 0.05f;
			else if (g >= 1.f)
				dg = -0.05f;

			if (b <= 0.0f)
				db = 0.05f;
			else if (b >= 1.f)
				db = -0.05f;

			r += dr;
			g += dg;
			b += db;
			mousePen->draw();

			/* Swap front and back buffers */
			glfwSwapBuffers(window);

			/* Poll for and process events */
			glfwPollEvents();
		}
    }
};