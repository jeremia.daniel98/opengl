#include "MousePen.h"

int MousePen::lines = 1;
std::vector<std::vector<glm::vec2> > MousePen::pos;
//VertexArray MousePen::va;
//VertexBuffer* MousePen::vb;

//Shader MousePen::shader("src/shaders/Basic.shader");
//
//double MousePen::mouseX = 0.0f;
//double MousePen::mouseY = 0.0f;
//bool MousePen::isClicked = false;

MousePen::MousePen()
	: shader("src/shaders/Basic.shader"), model(glm::mat4(1.f)) {
	shader.SetUniformMat4f("u_model", model);
	shader.SetUniformMat4f("u_cam", mainCamera.proj * mainCamera.view);
	shader.SetUniform4f("u_Color", 1.f, 1.f, 1.f, 1.f);
	pos.push_back(std::vector<glm::vec2>());
	/*model = glm::mat4(1.f);
	

	Bind();
	vb = new VertexBuffer(pos.data(), sizeof(float) * pos.size());
	VertexBufferLayout layout;
	layout.Push<float>(2);
	va.AddBuffer(*vb, layout);*/
}


bool isClicked = false;
void MousePen::mouseMoveEvent(GLFWwindow* window, double x, double y) {
	//std::cout << x << " " << 600 - y << std::endl;
	if (isClicked) {
		std::cout << x << " " << 600-y << std::endl;
		pos[MousePen::lines - 1].push_back(glm::vec2(x, 600 - y));
	}
}

void MousePen::mouseClickEvent(GLFWwindow* window, int button, int action, int mods) {
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		//std::cout << MousePen::mouseX << " " << MousePen::mouseY << std::endl;
		isClicked = true;
	}
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_REPEAT) {
		
	}
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
		isClicked = false;
		MousePen::lines++;
		pos.push_back(std::vector<glm::vec2>());
	}
}

void MousePen::setMouseMoveEvent(GLFWwindow* window) {
	glfwSetCursorPosCallback(window, mouseMoveEvent);
}

void MousePen::setMouseClickEvent(GLFWwindow* window) {
	glfwSetMouseButtonCallback(window, mouseClickEvent);
}

void MousePen::draw() {
	Bind();
	/*
	glDrawArrays(GL_LINES, 0, MousePen::pos.size() / 2);*/
	glLineWidth(10.0f);
	for (int line = 0; line < MousePen::lines; line++) {
		glBegin(GL_LINE_STRIP);
		glColor3f(0.8f, 0.2f, 0.5f);
		for (int i = 0; i < pos[line].size(); i++) {
			glVertex2f(pos[line][i].x, pos[line][i].y);
		}
		glEnd();
	}
	
}

void MousePen::Bind() {
	/*va.Bind();
	*/
	shader.Bind();
}
